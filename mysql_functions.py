#################################################
#  © Donovan Hare                               #
#                                               #
#################################################
import mysql.connector
import hashlib

print("Establishing the database connection...");
sql_connection = mysql.connector.connect(host='37.59.111.179', database='School', user='School_User', password='HpYJ8sATjdxAUZpK');
c = sql_connection.cursor();
print("Connected.");

def Login(user, pwd):
    print("Logging in...")
    hashedpwd = hashlib.md5(pwd.encode());
    query = ("SELECT SQLID FROM Accounts WHERE Username = '{0}' AND Password = '{1}' LIMIT 1".format(user, hashedpwd.hexdigest()))
    try:
        c.execute(query)
        data = c.fetchall()
        for row in data:
            global sqlid
            print("Details verified ({0}).".format(row[0]))
            sqlid = row[0]
            return sqlid
        print(row)
    except:
        print("Error: Incorrect username/password!")
        return 0

def CreateAccount(user, pwd):
    print("Creating account...");
    query = "INSERT INTO Accounts (Username, Password) VALUES (%s, %s)"
    try:
        hashedpwd = hashlib.md5(pwd.encode());
        c.execute(query, (user, hashedpwd.hexdigest()));
        print("Account {0} sucessfully created! You can now login to this account.".format(user));
        return 1;
    except:
        print("Error: Couldn't create the account.")
        return 0;

def CheckUsername(user):
    print("Checking username availability...")
    query = ("SELECT Username FROM Accounts WHERE Username = '{0}' LIMIT 1".format(user));
    try:
        c.execute(query);
        data = c.fetchall();
        for row in data:
            print("Username '{0}' has been taken.".format(row[0]));
            return 0;
        print(row)
    except:
        print("Username '{0}' is available!".format(user))
        return 1;

def submit_time(sqlid, score):
    query = "INSERT INTO Scoreboard (UserID, Score) VALUES (%s, %s)"
    try:
        c.execute(query, (sqlid, score));
        print("Score saved.");
        return 1;
    except:
        print("Error: Couldn't submit score.")
        return 0;

def load_highscore(sqlid):
    query = ("SELECT MIN(Score) FROM Scoreboard WHERE UserID = {0} LIMIT 1".format(sqlid))
    try:
        c.execute(query)
        data = c.fetchall()
        for row in data:
            return row[0]
        print(row)
    except:
        print("Error: Couldn't load highscore!")
        return 0
    
def close_connection():
    c.close();
    sql_connection.close();
