#################################################
#  © Donovan Hare                               #
#                                               #
#################################################

import sys
import time
import pygame
import random
import winsound

from threading import Thread
from aesthetic_functions import *
from mysql_functions import *


#=============================================================================#
#          -- > Variables                                                     #
#=============================================================================#

title = "Reaction Game"
done = False

t1 = 0
t2 = 0
f = 0
status = 0
high_score = 0.0

GAME_LOGIN = 0

string = []

GAME_IDLE = 1
GAME_STARTED = 2
GAME_REACT = 3
GAME_ENDED = 4

pygame.init()


#=============================================================================#
#          -- > Game Functions                                                #
#=============================================================================#

def update(): pygame.display.flip()

def quit():
    pygame.quit()
    close_connection()

def clearscreen():
    screen.fill([255,255,255])
    update()

def randscreen():
    randcolour=(random.randint(0, 255),random.randint(0, 255),random.randint(0, 255))
    screen.fill(randcolour)
    update()

def text(str, x, y):
    font = pygame.font.Font(None, 25)
    txt = font.render(str, True, [150,0,0])
    screen.blit(txt, [x,y])
    update()

def start_game():
    global status
    status = GAME_STARTED
    clearscreen()
    text("Game started!", 200, 70)
    text("Press 'SPACE' when the screen changes colour.", 60, 90)
    thread_function(1)
    
def end_game():
    global high_score, status
    status = GAME_ENDED
    t2 = time.time()
    ttp = (t2-t1) 
    penalty = (f / 10)
    total_score = (ttp + penalty)
    
    clearscreen()
    
    text("Time: {0} Penalty: {1}".format(round(ttp, 3), penalty), 125, 70)
    text("Total Time: {0}".format(round(total_score, 3)), 125, 90)
    
    if(total_score < high_score or high_score == 0.0):
        high_score = total_score
        text("HIGH SCORE!", 125, 110)
        print("High score:",high_score)
        winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
        
    submit_time(sqlid, total_score)
    thread_function(2)

def reset_game():
    global status, f, high_score
    status = GAME_IDLE
    f = 0
    clearscreen()
    text("Press 'SPACE' to start the game!", 120, 85)
    text("High Score: {0}".format(round(high_score, 3)), 340, 180)
    text("Press 'Q' to logout", 10, 180)
    winsound.PlaySound("SystemQuestion", winsound.SND_ALIAS)
                                  
def pause():
    try:
        time.sleep(5)
        reset_game()
    except:
        return 0

def thread_function(id):
    global trd1, trd2
    if(id == 1):
        trd1 = Thread(target=iniateclock, args=())
        trd1.start()
    if(id == 2):
        trd2 = Thread(target=pause, args=())
        trd2.start()

def iniateclock():
    global status, t1
    try:
        rand = random.randint(1,7)
        time.sleep(rand)
        t1 = time.time()
        randscreen()
        status = GAME_REACT
        text("GO!", 250, 90)
        winsound.PlaySound("SystemQuestion", winsound.SND_ALIAS)
    except:
        return 0

#=============================================================================#
#          -- > Account Classes                                               #
#=============================================================================#

class Button(object):
    
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        
        self.str = ""
        self.oX = 0
        self.oY = 0
        self.size = 12

    def draw(self):
        pygame.draw.rect(screen, [235,235,235], [self.x,self.y,self.w,self.h], 0)
        self.text(self.str,self.oX,self.oY)

    def clicked(self, pos):
        x = pos[0]
        y = pos[1]
        
        if (x > self.x
        and x < self.x + self.w
        and y > self.y
        and y < self.y + self.h):
            return 1


    def text(self, str, offsetX, offsetY):
        self.str = str
        self.oX = offsetX
        self.oY = offsetY
        
        font = pygame.font.Font(None, self.size)
        txt = font.render(self.str, True, [150,0,0])
        screen.blit(txt, [self.x + self.oX, self.y + self.oY])
        
        
    def select(self, selected):
        self.selected = selected
        if(self.selected == 1):
            pygame.draw.rect(screen, [0,0,0], [self.x,self.y,self.w+1,self.h+1], 0)
            self.draw()
            self.selected = 1
        else:
            self.selected = 0
            display_login()
            

class InputBox(Button):
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        
        self.oX = 5
        self.oY = 6
        self.size = 22

        self.selected = 0
        self.input = []
        self.str = ""

    def Key(self, key):
        if(key > 96 and key < 123 or key > 47 and key < 58):
            self.input.append(chr(key))
            
        elif(key == pygame.K_SPACE):
            self.input.append(" ")
            
        elif(key == pygame.K_BACKSPACE):
            self.input = self.input[:-1]

        elif(key == pygame.K_RETURN):
            submit_login()
            return

        
        self.str = ("".join(self.input))
        if(self.selected == 1): self.draw()
        update()

    def reset(self):
        self.str = ""
        del self.input[:]

#=============================================================================#
#          -- > Account Functions                                             #
#=============================================================================#

def length(str):
    if(len(str) >= 4 and len(str) <= 20): return 1
    return 0

def error(str):
    username.reset()
    password.reset()
    display_login()
    text("Error: " + str,50,20)


def show_login():
    global status
    status = GAME_LOGIN
    display_login()

        
def display_login():
    pygame.display.set_caption("{0}".format(title))
    clearscreen()
    
    text("Username:", 60, 65)
    text("Password:", 60, 95)

    username.draw()
    password.draw()
    
    register.draw()
    login.draw()

    login.text("Login", 13, 6)
    register.text("Register", 9, 6)
    
    update()


def submit_reg():
    if(length(username.str)):
        if(CheckUsername(username.str)):
            if(length(password.str)):
                CreateAccount(username.str, password.str)
                
            else:
                error("Password too short.")
        else:
            error("Username already taken.")
    else:
        error("Username too short.")

def submit_login():
    global sqlid, high_score
    sqlid = Login(username.str, password.str)
    if(sqlid > 0):
        pygame.display.set_caption("{0}  SQLID: {1}".format(title, sqlid))

        high_score = load_highscore(sqlid)
        if(high_score == None):
            high_score = 0
            
        reset_game()
    else:
        error("Username/Password incorrect.")
        


#=============================================================================#
#          -- > Game Loop                                                     #
#=============================================================================#

screen = pygame.display.set_mode((500,200),pygame.FULLSCREEN)
pygame.display.set_caption("{0}".format(title))
frame = pygame.time.Clock()
clearscreen()

login = Button(290,122,50,20)
register = Button(230,122,50,20)
username = InputBox(155,62,265,25)
password = InputBox(155,92,265,25)

display_login()
error("Please login/register below.")


while not done:
    for event in pygame.event.get():
        frame.tick(30)
        if event.type == pygame.QUIT:
            quit()
            done = True
            break
            
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                quit()
                done = True
                break
            
            if(status == GAME_LOGIN):
                if(username.selected == 1): username.Key(event.key)
                if(password.selected == 1): password.Key(event.key)
                if(event.key == pygame.K_TAB):
                    if(username.selected):
                        username.select(0)
                        password.select(1)
                        
                    elif(password.selected):
                        password.select(0)
                        username.select(1)
                        
                    else:
                        username.select(1)
                        password.select(0)
                    update()
                
            elif event.key == pygame.K_SPACE:                 
                if(status == GAME_IDLE):
                    start_game()
                    
                elif(status == GAME_STARTED):
                    print("Fault!")
                    f += 1
                    
                elif(status == GAME_REACT):
                    end_game()

                elif(status == GAME_ENDED):
                    print("Please wait...")
                    
            elif(event.key == pygame.K_q):
                if(status == GAME_IDLE):
                    show_login()
                    error("Please login/register below.")
                
        if event.type == pygame.MOUSEBUTTONDOWN:
            if(status == GAME_LOGIN):
                pos = pygame.mouse.get_pos()
                if(register.clicked(pos)):
                    login.select(0)
                    register.select(1)
                    submit_reg()
                    
                elif(login.clicked(pos)):
                    login.select(1)
                    register.select(0)
                    submit_login()
                    
                elif(username.clicked(pos)):
                    password.select(0)
                    username.select(1)
                    
                elif(password.clicked(pos)):
                    username.select(0)
                    password.select(1)
                    
                update()
                

quit()
