# Reaction Game #

This is my reaction game repository, designed to track the development of the game for 'Art of Programming' elective.

### What does this game feature? ###

* Accurate Reaction Timing
* Online Score Tracking
* [Scoreboard](http://donovanhare.co.uk/school/scoreboard.php)

### Requirements ###
* [Python  3.3](https://www.python.org/ftp/python/3.3.0/python-3.3.0.msi)
* [Pygame](https://bitbucket.org/pygame/pygame/downloads)
* [Mysql](http://cdn.mysql.com/Downloads/Connector-Python/mysql-connector-python-2.0.3-py3.3.msi)