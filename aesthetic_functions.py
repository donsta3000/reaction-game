#################################################
#  © Donovan Hare                               #
#                                               #
#################################################
def line(y):
    for l in range(0, y):
        print("")
    return

line(2)
print("-------------------------------------------------".center(75))
print("|                                               |".center(75))
print("|                          ©Donovan Hare - 2015 |".center(75))
print("-------------------------------------------------".center(75))
line(11)


def hline(): print("--------------------------------------------------------------------------------")

def header(l1,str,l2):
    l1 = int(l1)
    l2 = int(l2)
    y = 1
    x = 1
    while x <= l1:
        line(1)
        x += 1
    hline()
    print(str.upper())
    hline()
    while y <= l2:
        line(1)
        y += 1
